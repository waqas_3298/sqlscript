USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
UPDATE Baasjes 
SET Huisdieren_Id = 1 where Naam = 'Vincent';

UPDATE Baasjes 
SET Huisdieren_Id = 2 where Naam = 'Christiane';

UPDATE Baasjes 
SET Huisdieren_Id = 3 where Naam = 'Esther';

UPDATE Baasjes 
SET Huisdieren_Id = 4 where Naam = 'Bert';

UPDATE Baasjes 
SET Huisdieren_Id = 5 where Naam = 'Lyssa';
SET SQL_SAFE_UPDATES = 1;

SELECT * FROM Baasjes;