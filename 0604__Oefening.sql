CREATE TABLE Uitlenigen (
StartDatum DATE NOT  NULL,
EindDatum DATE NULL,
Leden_Id INT NOT NULL,
Boeken_Id INT NOT NULL,
CONSTRAINT fk_Uitleningen_Leden FOREIGN KEY (Leden_Id) REFERENCES Leden(Id),
CONSTRAINT fk_Uitleningen_Boeken FOREIGN KEY (Boeken_Id) REFERENCES Boeken(Id)
)