use aptunes;
drop procedure if exists DangerousInsertAlbumReleases;

delimiter $$

create procedure DangerousInsertAlbumReleases()

BEGIN
declare numberOfAlbums INT DEFAULT 0; 
declare numberOfBands INT DEFAULT 0;
declare randomAlbumId1 int default 0;
declare randomBandId1 int default 0;
declare randomAlbumId2 int default 0;
declare randomBandId2 int default 0;
declare randomAlbumId3 int default 0;
declare randomBandId3 int default 0;
declare randomValue tinyint;
declare teller int default 1;
declare exit handler for sqlexception
begin 
rollback;
select 'Nieuwe releases konden niet worden toegevoegd.';
end;
select count(*) into numberOfAlbums from albums;
select count(*) into numberOfBands from bands;
set randomAlbumId1 = floor(rand()* numberofAlbums) + 1;
set randomAlbumId2 = floor(rand()* numberofAlbums) + 1;
set randomAlbumId3 = floor(rand()* numberofAlbums) + 1;
set randomBandId1 = floor(rand()* numberofBands) + 1;
set randomBandId2 = floor(rand()* numberofBands) + 1;
set randomBandId3 = floor(rand()* numberofBands) + 1;
start transaction;
insert into albumreleases(bands_Id,Albums_Id)
values
	(randomBandId1,randomAlbumId1),
    (randomBandId2,randomAlbumId2);
	set randomValue = floor(rand() * 3) + 1;
    if randomValue = 1 then
		signal sqlstate '45000';
	end if;
    insert into albumereleases(Bands_Id,Album_Id)
		values	
		(randomBandId3,randomAlbumId3);
        commit;
	
END$$