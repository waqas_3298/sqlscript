USE aptunes;
DELIMITER $$
CREATE PROCEDURE NumberOfGenres(OUT aantal TINYINT)
BEGIN
	SELECT  COUNT(*)
    INTO aantal
    FROM Genres;
    
END $$
DELIMITER ;

