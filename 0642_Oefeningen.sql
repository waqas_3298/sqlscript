USE aptunes;
DROP PROCEDURE IF EXISTS CleanupOldMemberships;
DELIMITER $$
CREATE PROCEDURE CleanupOldMemberships (IN enddate DATE, OUT num INT)
BEGIN
START TRANSACTION;
select count(*)  into num
FROM Lidmaatschappen
Where Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < enddate;
SET SQL_SAFE_UPDATES = 0;
DELETE
from Lidmaatschappen
where Lidmaatschappen.Einddatum is not null and Midmaastschappen.Einddatum < enddate;
SET SQL_SAFE_UPDATES = 1;
COMMIT;
END$$
DELIMITER ;
