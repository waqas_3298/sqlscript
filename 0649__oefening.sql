use aptunes

drop procedure if exists DemonstraterHandlerOrder

delimiter $$
create procedure DemonstraterHandlerOrder()
begin
	declare randomValue tinyint default 0;
    declare continue handler for sqlstate '45002'
    begin 
		select 'State 45002 opgevangen. geen probleem.';
        end;
        declare continue handler for sqlexception
        begin
			resignal set message_text = 'ik heb mijn best gedaan!';
            end;
            set randomValue = floor(rand() * 3) + 1;
            if randomValue = 1 then
				signal sqlstate '45001';
			elseif randomValue = 2 then
				signal sqlstate '45002';
			else
				signal sqlstate '45003';
			end if;
            end$$
            delimiter ;
            