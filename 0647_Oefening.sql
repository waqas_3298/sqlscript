use aptunes;
drop procedure if exists MockAlbumReleasesLoop

delimiter $$
create procedure MockAlbumReleasesLoop(IN extraReleases INT)
begin	
declare counter int default 0;
declare succes bool;
callloop : loop
call MockAlbumReleaseWithSuccess(success);
	if success = 1
    then 
    set counter = counter+1;
    end if;
	if counter = extraReleases then
		leave callloop;
        end if;
    end loop;
end $$
delimiter ;
