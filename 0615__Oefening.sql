use ModernWays;
alter view auteursboeken
as
select concat(personen.Voornaam, ' ' , personen.Familienaam) as 'Auteur', boeken.Titel , Boeken.Id as 'Boeken_Id' from 
(boeken
inner join 
publicaties
on
Boeken_Id= Boeken.Id
inner join
personen
on
publicaties.Personen_Id=Personen.Id);