USE modernways;

SELECT studenten.id
FROM studenten
INNER JOIN evaluaties ON studenten.id = evaluaties.Studenten_Id
GROUP BY studenten.id
HAVING AVG(evaluaties.cijfer) > (SELECT AVG(cijfer) FROM evaluaties)