use aptunes

delimiter $$
create procedure GetAlbumDuration(IN album int , OUT totalDuration smallint unsigned)
begin
declare songDuration tinyint unsigned default 0;
declare ok bool default false;
declare songDurationCursor cursor for select Length from Liedjes where Album_Id = albums;
declare continue handler for not found set ok = true;
set TotalDuration = 0;
open songDurationCursor;
fetchloop: loop
	fetch songDurationCursor into songDuration;
    if ok = true then
		leave fetchloop;
        end if;
        set totalDuration = totalDuration + songDuration;
        end loop;
        close songDurationCursor;
	
end$$

delimiter ;
